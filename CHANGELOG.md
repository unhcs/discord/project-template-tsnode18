# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2022-05-31)


### Features

* update project-template-tsnode17 for node 18 ([a8dfb47](https://gitlab.com/unhcs/discord/project-template-tsnode18/commit/a8dfb47eccede0d29679db74776f2aa17c6a3516))
