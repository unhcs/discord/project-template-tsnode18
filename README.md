# project-template-tsnode18

The project includes ESLint (with Typescript integration), Prettier, lint-staged, commitlint, and Standard Version. Additionally, the package includes a basic Typescipt configuration for Node 18 (es2022) and the Jest unit testing framework.

Aside from the Typescript configuration, zero-minimal configuration is expected.

# Installation and Setup

Install package dependencies

```
npm install
```

Initialize Husky for pre-commit hocks

> Initialization of Husky requires the presents of a git repository. If a repository does not exist, one can be initialized using `git init` at the root of the project.

> If Git will not be used for the inheriting project, this step can be safely skipped.

```
npm run prepare
```

# Configuration

## VSCode

The project packages a VSCode recommendations file (i.e. `.vscode/extensions.json`) outing recommended extensions. When the project is opened for the first time, a dialog should be prompted recommended the installation of the listed packages. The recommended extensions are as follows:

| Extension name | Extension ID             | Description                                               |
| -------------- | ------------------------ | --------------------------------------------------------- |
| ESLint         | `dbaeumer.vscode-eslint` | Integration of ESLint results into VSCode problems panel. |
| Prettier       | `esbenp.prettier-vscode` | Add Prettier as a VSCode formatter.                       |

A workspace config (i.e. `.vscode/settings.json`) has also been packaged with the project. The config sets the formatter to Prettier and enables formatting on paste and save. It is not recommended these settings are modified.

## Jetbrains

// TODO

# Components

[ESLint](https://eslint.org/), used for static code analysis, is only configured to preform analysts, no styling.

The following ESLint plugins have been installed to expand the scope of the default ruleset:

| Plugin                                                                         | Description                                                 |
| ------------------------------------------------------------------------------ | ----------------------------------------------------------- |
| [eslint-comments](https://mysticatea.github.io/eslint-plugin-eslint-comments/) | Rules related to improper use of ESLint directive-comments. |
| [import](https://github.com/import-js/eslint-plugin-import)                    | Rules related to ES6+ import/export.                        |
| [jest](https://www.npmjs.com/package/eslint-plugin-jest)                       | Rules related to Jest unit test functionality and styling.  |
| [node](https://github.com/mysticatea/eslint-plugin-node)                       | Rules related to Node.js best practices.                    |

All code formatting is preformed using [Prettier](https://prettier.io/). Prettier is integrated into ESLint to avoid rule conflicts.

[lint-staged](https://github.com/okonet/lint-staged) is used to run ESLint and Prettier prior to a commit being made. If ESLint detects an error, the commit will be cancelled

> Although warnings will not result in a commit being cancelled, they should be considered prior to any commit.

[commitlint](https://commitlint.js.org/#/) preforms linting of commit messages against a standard, in the case of this project, [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/). The subset of allowed types is outlined in the [@commitlint/config-conventional documentation](https://github.com/conventional-changelog/commitlint/tree/master/%40commitlint/config-conventional), based on the [standards put forth by Angular](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines). A commit with a message not abiding by the configured standards will be cancelled.

[Standard Version](https://github.com/conventional-changelog/standard-version) is a versioning utility that provides a standardized means of signifying releases using the [semver](https://semver.org/) versioning standard. When a release is constructed, Standard Version automatically updates the package version and produces a `CHANGELOG` based on the commits (with messages following the Conventional Commits standard) made between the last release and the current release.

# Scripts

The `package.json` defines a number of scripts. All scripts can be run using `npm run <script>`

| Script    | Description                                                                                                                                                                                                                |
| --------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `prepare` | Post-install setup, at this time limited to the install of Husky. If the inheriting project will make use of Git, it is safe to move this to the `postinstall` script.                                                     |
| `test`    | Run all Jest unit tests.                                                                                                                                                                                                   |
| `format`  | Reformat all project files using Prettier.                                                                                                                                                                                 |
| `lint`    | Lint all project files using ESLint.                                                                                                                                                                                       |
| `release` | Create a new release using Standard Version. By default, a new minor version will be created. Reference the [documentation](https://github.com/conventional-changelog/standard-version#cli-usage) for available arguments. |
